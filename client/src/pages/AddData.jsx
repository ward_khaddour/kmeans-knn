/* eslint-disable react/prop-types */
import { useState } from "react";
import { BASE_URL } from "../config/constants";
import Spinner from "../components/Spinner";

const initialValues = {
  age: "",
  workclass: "",
  fnlwgt: "",
  education: "",
  educationNum: "",
  maritalStatus: "",
  occupation: "",
  relationship: "",
  race: "",
  sex: "",
  capitalGain: "",
  capitalLoss: "",
  hoursPerWeek: "",
  nativeCountry: "",
  income: "",
};

function AddDataForm({ onAdd }) {
  const [isLoading, setIsLoading] = useState(false);
  const [formData, setFormData] = useState(initialValues);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      const newRecord = {
        age: parseFloat(formData.age),
        workclass: formData.workclass,
        fnlwgt: parseFloat(formData.fnlwgt),
        education: formData.education,
        educationNum: parseFloat(formData.educationNum),
        maritalStatus: formData.maritalStatus,
        occupation: formData.occupation,
        relationship: formData.relationship,
        race: formData.race,
        sex: formData.sex,
        capitalGain: parseFloat(formData.capitalGain),
        capitalLoss: parseFloat(formData.capitalLoss),
        hoursPerWeek: parseFloat(formData.hoursPerWeek),
        nativeCountry: formData.nativeCountry,
        income: formData.income,
      };

      const response = await fetch(`${BASE_URL}/data`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newRecord),
      });

      const data = await response.json();
      onAdd(data);
      setFormData(initialValues);
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <div className="mb-6">
      <h2 className="text-2xl font-bold">Add New Data</h2>
      <form onSubmit={handleSubmit} className="space-y-4">
        <div>
          <label className="block">
            Age:
            <input
              type="number"
              name="age"
              value={formData.age}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Workclass:
            <input
              type="text"
              name="workclass"
              value={formData.workclass}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Fnlwgt:
            <input
              type="number"
              name="fnlwgt"
              value={formData.fnlwgt}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Education:
            <input
              type="text"
              name="education"
              value={formData.education}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Education-num:
            <input
              type="number"
              name="educationNum"
              value={formData.educationNum}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Marital Status:
            <input
              type="text"
              name="maritalStatus"
              value={formData.maritalStatus}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Occupation:
            <input
              type="text"
              name="occupation"
              value={formData.occupation}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Relationship:
            <input
              type="text"
              name="relationship"
              value={formData.relationship}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Race:
            <input
              type="text"
              name="race"
              value={formData.race}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Sex:
            <input
              type="text"
              name="sex"
              value={formData.sex}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Capital-gain:
            <input
              type="number"
              name="capitalGain"
              value={formData.capitalGain}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Capital-loss:
            <input
              type="number"
              name="capitalLoss"
              value={formData.capitalLoss}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Hours-per-week:
            <input
              type="number"
              name="hoursPerWeek"
              value={formData.hoursPerWeek}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Native Country:
            <input
              type="text"
              name="nativeCountry"
              value={formData.nativeCountry}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <div>
          <label className="block">
            Income:
            <input
              type="text"
              name="income"
              value={formData.income}
              onChange={handleChange}
              className="border rounded p-2 w-full"
              required
            />
          </label>
        </div>
        <button
          type="submit"
          className="bg-blue-600 text-white py-2 px-4 rounded"
        >
          {isLoading ? <Spinner /> : "Add Data"}
        </button>
      </form>
    </div>
  );
}

export default AddDataForm;
