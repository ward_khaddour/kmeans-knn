import { useState } from "react";
import KMeansForm from "../components/KMeansForm";

const Kmeans = () => {
  const [, setKmeansResults] = useState(null);

  return (
    <>
      <KMeansForm onResults={setKmeansResults} />
    </>
  );
};

export default Kmeans;
