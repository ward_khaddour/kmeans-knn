import { useState } from 'react'
import { BASE_URL } from '../config/constants'
import Spinner from '../components/Spinner'

function Predict() {
  const [k, setK] = useState(2)
  const [start, setStart] = useState(0)
  const [end, setEnd] = useState(10000)
  const [features, setFeatures] = useState({
    age: '',
    fnlwgt: '',
    educationNum: '',
    capitalGain: '',
    capitalLoss: '',
    hoursPerWeek: '',
  })
  const [isLoading, setIsLoading] = useState(false)
  const [assignedCluster, setAssignedCluster] = useState(null)

  const handleFeatureChange = e => {
    const { name, value } = e.target
    setFeatures({
      ...features,
      [name]: value,
    })
  }

  const handleSubmit = async e => {
    e.preventDefault()
    setIsLoading(true)

    const featureArray = Object.values(features).map(Number)

    try {
      const response = await fetch(`${BASE_URL}/kmeans/predict`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          k,
          start,
          end,
          newPoint: { features: featureArray },
        }),
      })

      const data = await response.json()
      setAssignedCluster(data.assignedCluster)
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <div>
      <h2 className='text-2xl font-bold'>K-Means Prediction</h2>
      <form
        onSubmit={handleSubmit}
        className='space-y-4'
      >
        <div>
          <label className='block'>
            k:
            <input
              type='number'
              value={k}
              onChange={e => setK(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Start:
            <input
              type='number'
              value={start}
              onChange={e => setStart(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            End:
            <input
              type='number'
              value={end}
              onChange={e => setEnd(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Age:
            <input
              type='number'
              name='age'
              value={features.age}
              onChange={handleFeatureChange}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Final Weight:
            <input
              type='number'
              name='fnlwgt'
              value={features.fnlwgt}
              onChange={handleFeatureChange}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Education Number:
            <input
              type='number'
              name='educationNum'
              value={features.educationNum}
              onChange={handleFeatureChange}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Capital Gain:
            <input
              type='number'
              name='capitalGain'
              value={features.capitalGain}
              onChange={handleFeatureChange}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Capital Loss:
            <input
              type='number'
              name='capitalLoss'
              value={features.capitalLoss}
              onChange={handleFeatureChange}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Hours per Week:
            <input
              type='number'
              name='hoursPerWeek'
              value={features.hoursPerWeek}
              onChange={handleFeatureChange}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <button
          type='submit'
          className='bg-blue-600 text-white py-2 px-4 rounded flex items-center justify-center'
        >
          {isLoading ? <Spinner /> : 'Predict Cluster'}
        </button>
      </form>
      {assignedCluster !== null && (
        <div className='mt-4'>
          <h3 className='text-xl font-bold'>Assigned Cluster:</h3>
          <p>{assignedCluster}</p>
        </div>
      )}
    </div>
  )
}

export default Predict
