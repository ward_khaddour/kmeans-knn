import { useEffect, useState } from 'react'
import { BASE_URL } from '../config/constants'

function DisplayData() {
  const [data, setData] = useState([])
  const [page, setPage] = useState(1)
  const [limit, setLimit] = useState(100)
  const [total, setTotal] = useState(0)

  useEffect(() => {
    fetch(`${BASE_URL}/data?page=${page}&limit=${limit}`)
      .then(response => response.json())
      .then(result => {
        setData(result.data)
        setTotal(result.total)
      })
  }, [page, limit])

  const handleNextPage = () => {
    if (page * limit < total) {
      setPage(page + 1)
    }
  }

  const handlePreviousPage = () => {
    if (page > 1) {
      setPage(page - 1)
    }
  }

  const handleLimitChange = e => {
    setLimit(Number(e.target.value))
    setPage(1)
  }

  return (
    <div>
      <h2 className='text-2xl font-bold'>Data Display</h2>
      <div className='mb-4'>
        <label className='mr-2'>Items per page:</label>
        <select
          value={limit}
          onChange={handleLimitChange}
          className='border rounded p-2'
        >
          <option value={10}>10</option>
          <option value={20}>20</option>
          <option value={50}>50</option>
          <option value={100}>100</option>
        </select>
      </div>
      <table className='min-w-full bg-white'>
        <thead>
          <tr>
            <th className='py-2'>Age</th>
            <th className='py-2'>Fnlwgt</th>
            <th className='py-2'>Workclass</th>
            <th className='py-2'>Education</th>
            <th className='py-2'>Education-num</th>
            <th className='py-2'>Occupation</th>
            <th className='py-2'>Relationship</th>
            <th className='py-2'>Race</th>
            <th className='py-2'>Sex</th>
            <th className='py-2'>Capital-gain</th>
            <th className='py-2'>Capital-loss</th>
            <th className='py-2'>Hours-per-week</th>
            <th className='py-2'>Income</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr
              key={index}
              className='text-center border-t'
            >
              <td className='py-2'>{item.features[0]}</td>
              <td className='py-2'>{item.features[1]}</td>
              <td className='py-2'>{item.features[2]}</td>
              <td className='py-2'>{item.features[3]}</td>
              <td className='py-2'>{item.features[4]}</td>
              <td className='py-2'>{item.features[5]}</td>
              <td className='py-2'>{item.features[6]}</td>
              <td className='py-2'>{item.features[7]}</td>
              <td className='py-2'>{item.features[8]}</td>
              <td className='py-2'>{item.features[9]}</td>
              <td className='py-2'>{item.features[10]}</td>
              <td className='py-2'>{item.features[11]}</td>
              <td className='py-2'>{item.label}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className='flex justify-between mt-4'>
        <button
          onClick={handlePreviousPage}
          disabled={page <= 1}
          className='bg-blue-500 text-white px-4 py-2 rounded disabled:opacity-50'
        >
          Previous
        </button>
        <button
          onClick={handleNextPage}
          disabled={page * limit >= total}
          className='bg-blue-500 text-white px-4 py-2 rounded disabled:opacity-50'
        >
          Next
        </button>
      </div>
    </div>
  )
}

export default DisplayData
