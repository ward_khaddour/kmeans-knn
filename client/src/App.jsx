import { Link, Outlet } from 'react-router-dom'

function App() {
  return (
    <div className='min-h-screen bg-gray-100'>
      <header className='bg-blue-600 text-white p-4'>
        <h1 className='text-center text-3xl font-bold'>KNN, K-Means</h1>
        <nav className='flex justify-center space-x-4'>
          <Link
            to='/'
            className='hover:underline'
          >
            Home
          </Link>
          <Link
            to='/add-data'
            className='hover:underline'
          >
            Add Data
          </Link>
          <Link
            to='/knn'
            className='hover:underline'
          >
            KNN
          </Link>
          <Link
            to='/kmeans'
            className='hover:underline'
          >
            K-Means
          </Link>
          <Link
            to='/predict/kmeans'
            className='hover:underline'
          >
            Predict K means
          </Link>
          <Link
            to='/predict/knn'
            className='hover:underline'
          >
            Predict Knn
          </Link>
        </nav>
      </header>

      <main className='container mx-auto p-4'>
        <Outlet />
      </main>
    </div>
  )
}

export default App
