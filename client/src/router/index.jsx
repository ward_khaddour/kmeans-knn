import { createBrowserRouter } from 'react-router-dom'
import App from '../App'
import DisplayData from '../pages/DisplayData'
import Kmeans from '../pages/Kmeans'
import Knn from '../pages/Knn'
import AddData from '../pages/AddData'
import KmeansPredict from '../pages/KmeansPredict'
import KnnPredict from '../pages/KnnPredict'

export const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        path: '',
        element: <DisplayData />,
      },
      {
        path: 'knn',
        element: <Knn />,
      },
      {
        path: 'kmeans',
        element: <Kmeans />,
      },
      {
        path: 'predict/kmeans',
        element: <KmeansPredict />,
      },
      {
        path: 'predict/knn',
        element: <KnnPredict />,
      },
      {
        path: 'add-data',
        element: <AddData />,
      },
    ],
  },
])
