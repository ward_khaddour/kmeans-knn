import { useState } from 'react'
import { BASE_URL } from '../config/constants'
import Spinner from './Spinner'

function KMeansForm() {
  const [k, setK] = useState(2)
  const [start, setStart] = useState(0)
  const [isLoading, setIsLoading] = useState(false)

  const [end, setEnd] = useState(10000)
  const [centroids, setCentroids] = useState([])
  const [clusters, setClusters] = useState([])
  const [iterations, setIterations] = useState(0)

  const handleSubmit = async e => {
    e.preventDefault()
    setIsLoading(true)
    try {
      const response = await fetch(`${BASE_URL}/kmeans`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ k, start, end }),
      })

      const data = await response.json()
      setCentroids(data.centroids)
      setClusters(data.clusters)
      setIterations(data.iterations)
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <div>
      <h2 className='text-2xl font-bold'>K-Means Algorithm</h2>
      <form
        onSubmit={handleSubmit}
        className='space-y-4'
      >
        <div>
          <label className='block'>
            k:
            <input
              type='number'
              value={k}
              onChange={e => setK(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Start:
            <input
              type='number'
              value={start}
              onChange={e => setStart(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            End:
            <input
              type='number'
              value={end}
              onChange={e => setEnd(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <button
          type='submit'
          className='bg-blue-600 text-white py-2 px-4 rounded flex items-center justify-center'
        >
          {isLoading ? <Spinner /> : 'Run K-means'}
        </button>
      </form>
      {centroids.length > 0 && (
        <div className='mt-4'>
          <h3 className='text-xl font-bold'>Centroids:</h3>
          <ul className='list-disc pl-5'>
            {centroids.map((centroid, index) => (
              <li key={index}>{centroid.join(', ')}</li>
            ))}
          </ul>
          <h3 className='text-xl font-bold'>Clusters:</h3>
          <ul className='list-disc pl-5'>
            {clusters.map((cluster, index) => (
              <li key={index}>
                Cluster {index + 1}: {cluster.length} points
              </li>
            ))}
          </ul>
          {/* <h3 className="text-xl font-bold">Iterations: {iterations}</h3> */}
        </div>
      )}
    </div>
  )
}

export default KMeansForm
