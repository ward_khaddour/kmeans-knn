const Spinner = () => {
  return (
    <div className="flex justify-center items-center">
      <div
        className="animate-spin rounded-full h-5 w-5 border-4 border-t-blue-500 border-e-blue-500
       border-b-blue-500 border-s-transparent"
      ></div>
    </div>
  );
};

export default Spinner;
