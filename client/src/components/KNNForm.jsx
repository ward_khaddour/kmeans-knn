import { useState } from 'react'
import { BASE_URL } from '../config/constants'
import Spinner from './Spinner'

function KNNForm() {
  const [k, setK] = useState(3)
  const [isLoading, setIsLoading] = useState(false)
  const [start, setStart] = useState(0)
  const [end, setEnd] = useState(8000)
  const [predictions, setPredictions] = useState([])
  const [accuracy, setAccuracy] = useState('')
  const [neighbors, setNeighbors] = useState([])
  const [confusionMatrix, setConfusionMatrix] = useState([])

  const handleSubmit = async e => {
    e.preventDefault()
    setIsLoading(true)
    try {
      const response = await fetch(`${BASE_URL}/knn`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ k, start, end }),
      })

      const data = await response.json()
      setPredictions(data.predictions)
      setAccuracy(data.accuracy)
      setNeighbors(data.neighbors)
      setConfusionMatrix(data.confusionMatrix)
    } catch (err) {
      console.log(err)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <div>
      <h2 className='text-2xl font-bold'>KNN Algorithm</h2>
      <form
        onSubmit={handleSubmit}
        className='space-y-4'
      >
        <div>
          <label className='block'>
            k:
            <input
              type='number'
              value={k}
              onChange={e => setK(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            Start:
            <input
              type='number'
              value={start}
              onChange={e => setStart(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <div>
          <label className='block'>
            End:
            <input
              type='number'
              value={end}
              onChange={e => setEnd(e.target.value)}
              className='border rounded p-2 w-full'
            />
          </label>
        </div>
        <button
          type='submit'
          className='bg-blue-600 text-white py-2 px-4 rounded flex items-center justify-center'
        >
          {isLoading ? <Spinner /> : 'Run KNN'}
        </button>
      </form>

      {predictions.length > 0 && (
        <div className='mt-4'>
          <h3 className='text-xl font-bold'>Accuracy: {accuracy}</h3>
          <h3 className='text-xl font-bold'>Predictions:</h3>
          <ul className='list-disc pl-5'>
            {predictions.map((prediction, index) => (
              <li key={index}>{prediction}</li>
            ))}
          </ul>
          <h3 className='text-xl font-bold'>Neighbors:</h3>
          <ul className='list-disc pl-5'>
            {neighbors.map((neighbor, index) => (
              <li key={index}>
                Test Point: {neighbor.testPoint.join(', ')} - Neighbors:{' '}
                {neighbor.neighbors
                  .map(n => `${n.label} (${n?.distance?.toFixed(2)})`)
                  .join(', ')}
              </li>
            ))}
          </ul>
          <h3 className='text-xl font-bold'>Confusion Matrix:</h3>
          {/* <pre>{JSON.stringify(confusionMatrix, null, 2)}</pre> */}
        </div>
      )}
    </div>
  )
}

export default KNNForm
