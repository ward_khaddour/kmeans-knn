class KMeans {
  constructor(k, maxIterations = 100) {
    this.k = k
    this.maxIterations = maxIterations
    this.centroids = []
    this.data = []
  }

  fit(data) {
    this.data = data
    this.centroids = this.initializeCentroids(data)

    for (let i = 0; i < this.maxIterations; i++) {
      const clusters = this.createClusters(this.data)

      const newCentroids = clusters.map(cluster => {
        if (cluster.length === 0)
          return this.centroids[clusters.indexOf(cluster)]
        const mean = Array(cluster[0].features.length).fill(0)
        cluster.forEach(item => {
          item.features.forEach((value, index) => {
            mean[index] += value
          })
        })

        return mean.map(sum => sum / cluster.length)
      })

      if (this.hasConverged(this.centroids, newCentroids)) {
        break
      }

      this.centroids = newCentroids
    }
  }

  initializeCentroids(data) {
    const shuffled = data.sort(() => 0.5 - Math.random())
    return shuffled.slice(0, this.k).map(item => item.features)
  }

  createClusters(data) {
    const clusters = Array.from({ length: this.k }, () => [])
    data.forEach(item => {
      const distances = this.centroids.map(centroid =>
        this.euclideanDistance(item.features, centroid)
      )
      const clusterIndex = distances.indexOf(Math.min(...distances))
      clusters[clusterIndex]
        ? clusters[clusterIndex].push(item)
        : (clusters[clusterIndex] = [item])
    })

    return clusters
  }

  euclideanDistance(point1, point2) {
    return Math.sqrt(
      point1.reduce((sum, value, index) => {
        return sum + Math.pow(value - point2[index], 2)
      }, 0)
    )
  }

  hasConverged(oldCentroids, newCentroids) {
    return oldCentroids.every((centroid, index) =>
      centroid.every(
        (value, i) => Math.abs(value - newCentroids[index][i]) < 1e-4
      )
    )
  }

  predict(newDataPoint) {
    const distances = this.centroids.map(centroid =>
      this.euclideanDistance(newDataPoint.features, centroid)
    )
    const nearestClusterIndex = distances.indexOf(Math.min(...distances))
    return nearestClusterIndex
  }
}

module.exports = KMeans
