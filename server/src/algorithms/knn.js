class KNN {
  constructor(k) {
    this.k = k
    this.trainingData = []
  }

  fit(trainingData) {
    this.trainingData = trainingData
  }

  predict(testData) {
    return testData.map(testPoint => {
      const neighbors = this.getNeighbors(testPoint)
      const labels = neighbors.map(neighbor => neighbor.label)
      return this.mode(labels)
    })
  }

  predictSingle(testPoint) {
    const neighbors = this.getNeighbors(testPoint)
    const labels = neighbors.map(neighbor => neighbor.label)
    return this.mode(labels)
  }

  getNeighbors(testPoint) {
    const distances = this.trainingData.map(trainPoint => {
      return {
        distance: this.euclideanDistance(
          trainPoint.features,
          testPoint.features
        ),
        label: trainPoint.label,
      }
    })

    distances.sort((a, b) => a.distance - b.distance)

    return distances.slice(0, this.k)
  }

  euclideanDistance(point1, point2) {
    if (!Array.isArray(point1) || !Array.isArray(point2)) {
      throw new Error('Points must be arrays')
    }
    return Math.sqrt(
      point1.reduce((sum, value, index) => {
        return sum + Math.pow(value - point2[index], 2)
      }, 0)
    )
  }

  mode(array) {
    const frequency = {}
    array.forEach(value => {
      if (frequency[value]) {
        frequency[value]++
      } else {
        frequency[value] = 1
      }
    })

    let maxFrequency = 0
    let modeValue = null
    for (const key in frequency) {
      if (frequency[key] > maxFrequency) {
        maxFrequency = frequency[key]
        modeValue = key
      }
    }
    return modeValue
  }

  static calculateConfusionMatrix(predictions, actualLabels, classes = []) {
    const confusionMatrix = {}

    for (let i = 0; i < actualLabels.length; i++) {
      if (confusionMatrix[actualLabels[i]] === undefined) {
        confusionMatrix[actualLabels[i]] = {}
      }

      if (confusionMatrix[actualLabels[i]][predictions[i]] === undefined) {
        confusionMatrix[actualLabels[i]][predictions[i]] = 0
      }

      confusionMatrix[actualLabels[i]][predictions[i]]++
    }

    const matrix = []
    classes.forEach(actualClass => {
      const row = []
      classes.forEach(predictedClass => {
        row.push(confusionMatrix[actualClass]?.[predictedClass] || 0)
      })
      matrix.push(row)
    })

    return matrix
  }

  static calculateAccuracy(predictions, actualLabels) {
    let correct = 0
    predictions.forEach((prediction, index) => {
      if (prediction === actualLabels[index]) {
        correct++
      }
    })
    return (correct / predictions.length) * 100
  }
}

module.exports = KNN
