const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const router = require('./router')

const app = express()

const port = process.env.PORT || 3000

app.use(bodyParser.json())

app.use(cors({ origin: '*' }))

app.use(router)

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`)
})
