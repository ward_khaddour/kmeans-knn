const fs = require('fs')
const fullDataset = require('../dataset/fullDataset')
const { DATASET_FILE_NAME } = require('../constatnts')

const dataService = {
  get: (page, limit) => {
    const startIndex = (page - 1) * limit
    const endIndex = startIndex + parseInt(limit)

    const result = fullDataset.slice(startIndex, endIndex)

    return {
      page: parseInt(page),
      limit: parseInt(limit),
      total: fullDataset.length,
      data: result,
    }
  },
  add: newRecord => {
    const data = JSON.parse(fs.readFileSync(DATASET_FILE_NAME, 'utf-8'))
    data.push(newRecord)
    fs.writeFileSync('census_income_10000.json', JSON.stringify(data, null, 2))
    return newRecord
  },
}

module.exports = dataService
