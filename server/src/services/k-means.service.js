const KMeans = require('../algorithms/kmeans')
const dataset = require('../dataset')

const kmeansService = {
  execute: (k, start, end) => {
    const data = dataset.slice(start, end)

    const kmeans = new KMeans(k)
    kmeans.fit(data)

    const clusters = kmeans.createClusters(data)

    return {
      centroids: kmeans.centroids,
      clusters: clusters.map(cluster => cluster.map(item => item.features)),
      iterations: kmeans.iterations,
    }
  },

  predict: (k, start, end, newPoint) => {
    const data = dataset.slice(start, end)

    const kmeans = new KMeans(k)
    kmeans.fit(data)

    const assignedCluster = kmeans.predict(newPoint)

    return {
      assignedCluster,
      newPoint: newPoint.features,
    }
  },
}

module.exports = kmeansService
