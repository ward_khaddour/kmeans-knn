// knnService.js
const KNN = require('../algorithms/knn')
const dataset = require('../dataset')
const knnService = {
  execute: (k, start, end) => {
    const trainData = dataset.slice(start, end)
    const testData = dataset
      .slice(end)
      .map(item => ({ features: item.features, label: item.label }))

    const knn = new KNN(k)
    knn.fit(trainData)

    const predictions = knn.predict(testData)
    const actualLabels = testData.map(item => item.label)
    const accuracy = KNN.calculateAccuracy(predictions, actualLabels)

    const neighbors = testData.map((testPoint, index) => ({
      testPoint: testPoint.features,
      neighbors: knn.getNeighbors(testPoint),
    }))

    const confusionMatrix = KNN.calculateConfusionMatrix(
      predictions,
      actualLabels
    )

    return {
      accuracy: `${accuracy.toFixed(2)}%`,
      predictions,
      neighbors,
      confusionMatrix,
    }
  },
  predict: (k, newPoint) => {
    const trainData = dataset
    const knn = new KNN(k)
    knn.fit(trainData)
    const prediction = knn.predictSingle({ features: newPoint })
    return {
      prediction,
    }
  },
}

module.exports = knnService
