const fs = require('fs')
const { DATASET_FILE_NAME } = require('../constatnts')

const data = JSON.parse(fs.readFileSync(DATASET_FILE_NAME, 'utf-8'))

function loadDataset() {
  return data.map(item => {
    return {
      features: [
        parseFloat(item.age),
        parseFloat(item.fnlwgt),
        parseFloat(item['education-num']),
        parseFloat(item['capital-gain']),
        parseFloat(item['capital-loss']),
        parseFloat(item['hours-per-week']),
      ],
      label: item.income,
    }
  })
}

const dataset = loadDataset()

module.exports = dataset
