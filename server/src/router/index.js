const { Router } = require('express')
const dataService = require('../services/data.service')
const knnService = require('../services/knn.service')
const kmeansService = require('../services/k-means.service')

const router = Router()

router.get('/data', (req, res) => {
  const { page = 1, limit = 100 } = req.query

  const data = dataService.get(page, limit)

  res.status(200).json(data)
})

router.post('/data', (req, res) => {
  const {
    age,
    workclass,
    fnlwgt,
    education,
    educationNum,
    maritalStatus,
    occupation,
    relationship,
    race,
    sex,
    capitalGain,
    capitalLoss,
    hoursPerWeek,
    nativeCountry,
    income,
  } = req.body

  if (
    age === undefined ||
    workclass === undefined ||
    fnlwgt === undefined ||
    education === undefined ||
    educationNum === undefined ||
    maritalStatus === undefined ||
    occupation === undefined ||
    relationship === undefined ||
    race === undefined ||
    sex === undefined ||
    capitalGain === undefined ||
    capitalLoss === undefined ||
    hoursPerWeek === undefined ||
    nativeCountry === undefined ||
    income === undefined
  ) {
    return res.status(400).json({ message: 'All fields are required' })
  }

  const newRecord = {
    age: parseFloat(age),
    workclass,
    fnlwgt: parseFloat(fnlwgt),
    education,
    educationNum: parseFloat(educationNum),
    maritalStatus,
    occupation,
    relationship,
    race,
    sex,
    capitalGain: parseFloat(capitalGain),
    capitalLoss: parseFloat(capitalLoss),
    hoursPerWeek: parseFloat(hoursPerWeek),
    nativeCountry,
    income,
  }

  const addedRecord = dataService.add(newRecord)
  res.status(201).json(addedRecord)
})

router.post('/knn', (req, res) => {
  const { k, start, end } = req.body
  const data = knnService.execute(k, start, end)
  res.status(200).json(data)
})

router.post('/kmeans', (req, res) => {
  const { k, start, end } = req.body
  const data = kmeansService.execute(k, start, end)

  res.status(200).json(data)
})

router.post('/kmeans/predict', (req, res) => {
  const { k, start, end, newPoint } = req.body

  if (
    !newPoint ||
    !Array.isArray(newPoint.features) ||
    newPoint.features.length === 0
  ) {
    return res
      .status(400)
      .json({ message: 'New data point must be provided with features array' })
  }

  const prediction = kmeansService.predict(k, start, end, newPoint)

  res.status(200).json(prediction)
})

router.post('/knn/predict', (req, res) => {
  const { k, newPoint } = req.body

  if (
    !newPoint ||
    !Array.isArray(newPoint.features) ||
    newPoint.features.length === 0
  ) {
    return res
      .status(400)
      .json({ message: 'New data point must be provided with features array' })
  }

  const prediction = knnService.predict(k, newPoint.features)

  res.status(200).json(prediction)
})

module.exports = router
