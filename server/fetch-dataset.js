const fs = require('fs')
const axios = require('axios')
const csv = require('csvtojson')
const path = require('path')

const url =
  'https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data'

const columns = [
  'age',
  'workclass',
  'fnlwgt',
  'education',
  'education-num',
  'marital-status',
  'occupation',
  'relationship',
  'race',
  'sex',
  'capital-gain',
  'capital-loss',
  'hours-per-week',
  'native-country',
  'income',
]

axios
  .get(url)
  .then(response => {
    csv({ noheader: true, headers: columns })
      .fromString(response.data)
      .then(json => {
        const sampledJson = json.slice(0, 10000)

        fs.writeFileSync(
          path.join(__dirname, 'census_income_10000.json'),
          JSON.stringify(sampledJson, null, 2)
        )
        console.log('Dataset has been saved as census_income_10000.json')
      })
  })
  .catch(err => {
    console.error('Error fetching the dataset:', err)
  })
