# K-Means, KNN algorithms

Small app to display dataset, run K-Means and KNN algorithm to this dataset, and add new records to the dataset

## Requirements

You need Node.js >= 20 installed on your machine, visit [Node.js](https://nodejs.org/)

### Run the server

```bash
    cd server
    npm install
    npm run dev
```

### Run the client app

```bash
    cd client
    npm install
    npm run dev
```

### Preview the app

Run client and server apps, then open [http://localhost:5173](http://localhost:5173)
